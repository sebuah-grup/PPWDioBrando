from django.shortcuts import render
from app_status.models import Status
from app_friends.models import Friend
from app_profile.models import Edit

# Create your views here.
response = {}
def index(request):
    if(Edit.objects.all().count() == 0):
        editan = Edit(name = 'Dio Brando', birthday = '01 Jan', gender = 'Male', description = 'Omae Wa Mou Shindeiru', img = 'https://i.imgur.com/9V9UJMi.jpg', email = 'dio.brando@diobrando.com')
        editan.save()
    status_count = Status.objects.all().count()
    friend_count = Friend.objects.all().count()
    last_status = Status.objects.all().last()
    user = Edit.objects.last()
    
    response = {'profile_pict' : user.img, 'user_name' : user.name, 'status_count' : status_count,
                "friend_count" : friend_count, 'last_status' : last_status}
    return render(request, 'dashboard-base.html', response)
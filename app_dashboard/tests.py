from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from app_status.models import Status
from app_friends.models import Friend

# Create your tests here.
class DashboardUnitTest(TestCase):
    def test_dashboard_page_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_current_status_count_showed_in_dashboard(self):
        statusTest = 'my name is usep'
        new_status = Status.objects.create(status=statusTest)
        
        status_count = Status.objects.all().count()
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(str(status_count)+" Posts", html_response)
    
    def test_current_friends_count_showed_in_dashboard(self):
        new_friend = Friend.objects.create(name='Joseph Jotaro', heroku ='http://test.herokuapp.com')
        
        friend_count = Friend.objects.all().count()
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(str(friend_count)+" People", html_response)
    
    def test_latest_post_showed_in_dashboard(self):
        statusTest = 'my name is jeff XDDDD'
        new_status = Status.objects.create(status=statusTest)
        
        last_status = Status.objects.all().last()
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(statusTest, html_response)
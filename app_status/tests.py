from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status

# Create your tests here.
class StatusUnitTest(TestCase):
    def test_status_page_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)
		
    def test_add_status_redirect_to_status(self):
        response = Client().get('/status/add_status/')
        self.assertEqual(response.status_code,301)

    def test_main_page_redirects_to_status_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,301)

    def test_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_add_one_message_render(self):
        statusTest = 'aaaaaaaaaaaaaaaaaa'
        postStatus = self.client.post('/status/add_status/', {'status' : statusTest})

        self.assertEqual(postStatus.status_code, 301)

        responsePage = self.client.get('/status/')
        html_response = responsePage.content.decode('utf8')
        self.assertIn(statusTest, html_response)
    
    def test_remove_one_message(self):
        message = 'my name is jeff'
        new_activity = Status.objects.create(status = message)
        deleteJeff = self.client.post('/status/delete_status/', {'status_id' : '1'})

        self.assertEqual(deleteJeff.status_code, 301)

        responsePage = deleteJeff.content.decode('utf8')
        self.assertNotIn(message, responsePage)

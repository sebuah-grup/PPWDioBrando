from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from app_profile.models import Edit
from .forms import Add_Status_Form
# Create your views here.

# Response for the html
response = {}
def index(request):
    if(Edit.objects.all().count() == 0):
        editan = Edit(name = 'Dio Brando', birthday = '01 Jan', gender = 'Male', description = 'Omae Wa Mou Shindeiru', img = 'https://i.imgur.com/9V9UJMi.jpg', email = 'dio.brando@diobrando.com')
        editan.save()
    user = Edit.objects.last()
    response = {'profile_pict' : user.img, 'user_name' : user.name, 'status' : Status.objects.all().values(), 'add_status_form' : Add_Status_Form}
    return render(request, 'status-base.html', response)

def add_status(request):
    form = Add_Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        status_now = Status(status=request.POST['status'])
        response['status'] = request.POST['status']
        status_now.save()
        html = 'status-show.html'
    return redirect('/status/', permanent = True)

def delete_status(request):
    if request.method == 'POST':
        inventory = Status.objects.last()
        status_id = int(request.POST['status_id'])
        status_to_delete = Status.objects.get(id=status_id)
        status_to_delete.delete()
        return redirect('/status/', permanent = True)
from django import forms

class Add_Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form_control',
        'style' : 'resize:none',
        'rows' : '3',
    }

    status = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, label='', max_length=140)
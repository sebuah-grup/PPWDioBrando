from django.conf.urls import url
from .views import add_status, index, delete_status

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status/', add_status, name='add_status'),
    url(r'^delete_status/', delete_status, name='delete_status')
]

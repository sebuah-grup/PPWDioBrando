# This Python script saves general user information
users = {
	'Dio Brando':{
		'Profile Image' : 'img/profile.jpg',
		'Birthday' : '01 Jan',
		'Gender' : 'Male',
		'Expertise' : ["Vampiric", "Godly", "Badboy"],
		'Description' : 'Omae wa Mou Shindeiru',
		'Email' : 'dio.brando@diobrando.com'
	},
}
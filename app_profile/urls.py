from django.conf.urls import url
from .views import index, edit_profile, edit_dong
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit-profile', edit_profile, name='edit-profile'),
    url(r'^edit_dong', edit_dong, name='edit_dong'),
]

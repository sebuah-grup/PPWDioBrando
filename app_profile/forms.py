from django import forms
#from .models import Edit

class Edit_In(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    #all_friend = Edit.objects.last()
    name = forms.CharField(label='Nama', required=True, max_length=27, widget=forms.TextInput(attrs=attrs), initial = "Dio Brando")
    birthday = forms.CharField(label = 'Birthday', required=True, max_length=6,  widget=forms.TextInput(attrs=attrs), initial = "01 Jan")
    gender = forms.CharField(label = 'Gender', required= True, max_length=6,  widget=forms.TextInput(attrs=attrs), initial =  "Male")
    description = forms.CharField(label = 'Description', required= True, max_length = 140,  widget=forms.Textarea(attrs=attrs), initial = "Omae Wa Mou Shindeiru")
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs=attrs), initial = "dio.brando@diobrando.com") 
    img = forms.URLField(label = 'Profile Image URL', required = True,   widget = forms.URLInput(attrs = attrs), initial = "https://i.imgur.com/9V9UJMi.jpg")

from django.db import models

# Create your models here.


class Edit(models.Model):
    name = models.CharField(max_length=27)
    birthday = models.CharField(max_length=6)
    gender = models.CharField(max_length=6)
    description = models.TextField(max_length=140)
    email = models.EmailField()
    img = models.URLField()

    def __str__(self):
        return self.name + self.birthday + self.gender + self.description + self.email + self.img
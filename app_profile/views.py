from django.shortcuts import render
from diobrando.users import users
from .models import Edit
from .forms import Edit_In
from django.http import HttpResponseRedirect



# Create your views here.


user_name = "Dio Brando"
user_dict = users[user_name]
profile_dict = [{'subject':x,'value':user_dict[x]} for x in user_dict if x != 'Profile Image']
response  = {'birthday' : '01 Jan', 'gender' : 'Male',  'description' : 'Omae Wa Mou Shindeiru', 'user_name' : user_name, 
'email' : 'dio.brando@diobrando.com', 'profile_pict' : 'https://i.imgur.com/9V9UJMi.jpg', 'expertise': ['Vampiric', 'Godly', 'Badboy'] }

def index(request):
	if(Edit.objects.all().count() == 0):
		editan = Edit(name = 'Dio Brando', birthday = '01 Jan', gender = 'Male', description = 'Omae Wa Mou Shindeiru', img = 'https://i.imgur.com/9V9UJMi.jpg', email = 'dio.brando@diobrando.com')
		editan.save()
	return render(request, 'app_profile/profile-base.html', response)


def edit_profile(request):
	if(Edit.objects.all().count() == 0):
		editan = Edit(name = user_name, birthday = '01 Jan', gender = 'Male', description = 'Omae Wa Mou Shindeiru', img = 'https://i.imgur.com/9V9UJMi.jpg', email = 'dio.brando@diobrando.com')
		editan.save()
	user = Edit.objects.last()
	response["Edit_In"] = Edit_In(initial = {'name': user.name, 'birthday' : user.birthday, 'email' : user.email, 
		'gender': user.gender, 'description' : user.description, 'profile_pict' : user.img})
	return render(request, 'app_profile/edit-profile.html', response)

def edit_dong(request):
	form = Edit_In(request.POST or None)
	response['expertise'] = []
	vampire = request.POST.get('expertise1')
	godly = request.POST.get('expertise2')
	badboy = request.POST.get('expertise3')
	inhuman = request.POST.get('expertise4')
	regeneration = request.POST.get('expertise5')
	enhanced = request.POST.get('expertise6')

	user_name = request.POST['name'] 
	response['user_name'] = user_name
	response['birthday'] = request.POST['birthday'] 
	response['email'] = request.POST['email']
	response['gender'] = request.POST['gender']
	response['description'] = request.POST['description']
	response['profile_pict'] = request.POST['img']
	if(request.method == 'POST' and form.is_valid()):
		if 'vampiric' == vampire:
			response['expertise'].append('Vampiric')
		if  'godly' == godly:
			response['expertise'].append('Godly')
		if 'badboy' == badboy:
			response['expertise'].append('Badboy')
		if 'inhuman' == inhuman:
			response['expertise'].append('Inhuman Speed and Strength')
		if 'regeneration' == regeneration:
			response['expertise'].append('Regeneration')
		if 'enhanced' == enhanced:
			response['expertise'].append('Enhanced Senses')
		editan = Edit(name = response['user_name'], birthday = response['birthday'], gender = response['gender'], description = response['description'], 
			img = response['profile_pict'], email = response['email'])
		editan.save()
	return HttpResponseRedirect('/profile/')
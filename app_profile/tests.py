from django.test import TestCase
from django.test import Client
from django.urls import resolve
from diobrando.users import users
from django.http import HttpRequest
from .views import index
from .views import user_name, edit_dong, edit_profile
from .models import Edit
from .forms import Edit_In
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_page_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_profile_using_profile_base(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'app_profile/profile-base.html')

    def test_model_can_create_new_object(self):
        new_object = Edit.objects.create(name='Dio Bagong',email='test@gmail.com',description='This is a test', 
        	img = 'https://i.imgur.com/X8rM1t7.png', birthday = '01 Jan', gender = 'Male')
        counting_all_available_message= Edit.objects.all().count()
        self.assertIn('This is a test', str(new_object))
        self.assertIn('Dio Bagong', str(new_object))
        self.assertIn('test@gmail.com', str(new_object))
        self.assertIn('01 Jan', str(new_object))
        self.assertIn('https://i.imgur.com/X8rM1t7.png', str(new_object))
        self.assertIn('Male', str(new_object))
        self.assertEqual(counting_all_available_message,1)

    def test_edit_profile_url_exist(self):
        response = Client().get('/profile/edit-profile')
        self.assertEqual(response.status_code, 200)

    def test_edit_profile_using_edit_profile_html(self):
        response = Client().get('/profile/edit-profile')
        self.assertTemplateUsed(response, 'app_profile/edit-profile.html')

    def test_edit_profile_using_edit_dong_func(self):
        found = resolve('/profile/edit_dong')
        self.assertEqual(found.func, edit_dong)

    def test_edit_profile_fail(self):
        response = Client().post('/profile/edit-profile', {'name': '', 'birthday': '', 'gender' : '', 'description': '',
            'email' : '', 'profile_pict' : ''})
        self.assertEqual(response.status_code, 200)

    def test_form_validation_for_blank_items(self):
        form = Edit_In(data={'name': '', 'birthday': '', 'gender' : '', 'description': '',
            'email' : '', 'profile_pict' : ''})
        self.assertFalse(form.is_valid())

    def test_edit_profile_success(self):
        name =  "Dio Brando"
        birthday = "01 Jan"
        gender = "Male"
        desc = "Omae Wa Mou Shindeiru"
        email = "dio.brando@diobrando.com"
        profile_pict = "https://i.imgur.com/9V9UJMi.jpg"
        form = Edit_In(data={'name': name, 'birthday': birthday, 'gender' : gender, 'description': desc,
            'email' : email, 'profile_pict' : profile_pict})
        response = Client().post('/profile/edit_dong/', {'name': name, 'birthday': birthday, 'gender' : gender, 'description': desc,
            'email' : email, 'img' : profile_pict, 'expertise1' : 'vampiric', 'expertise2' : 'godly', 'expertise3' : 'badboy', 'expertise4' : 'inhuman', 
            'expertise5' : 'regeneration', 'expertise6' : 'enhanced'}, follow = True)
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(name,html_response)
        self.assertIn(birthday,html_response)
        self.assertIn(gender,html_response)
        self.assertIn(desc,html_response)
        self.assertIn(email,html_response)
        self.assertIn(profile_pict,html_response)



class App_Profile_FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(App_Profile_FunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(App_Profile_FunctionalTest, self).tearDown()

    def test_existence(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/edit-profile')

        name = selenium.find_element_by_id('id_name')
        gender = selenium.find_element_by_id('id_gender')
        email = selenium.find_element_by_id('id_email')
        img = selenium.find_element_by_id('id_img')
        birthday = selenium.find_element_by_id('id_birthday')
        description = selenium.find_element_by_id('id_description')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        name.send_keys('Dio Brando')
        gender.send_keys('Male')
        email.send_keys('dio.brando@diobrando.com')
        img.send_keys('https://i.imgur.com/9V9UJMi.jpg')
        birthday.send_keys('01 Jan')
        description.send_keys('Halo semua, apa kabar?')

        # submitting the form
        submit.send_keys(Keys.RETURN)



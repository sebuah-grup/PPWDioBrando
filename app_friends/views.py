from django.shortcuts import render, redirect
from .forms import Add_Friend_Form
from .models import Friend
from app_profile.models import Edit
from django.http import HttpResponseRedirect
import urllib

# Create your views here.
response = {}
def index(request):
    response = {}
    if(Edit.objects.all().count() == 0):
        editan = Edit(name = 'Dio Brando', birthday = '01 Jan', gender = 'Male', description = 'Omae Wa Mou Shindeiru', img = 'https://i.imgur.com/9V9UJMi.jpg', email = 'dio.brando@diobrando.com')
        editan.save()
    all_friend = Friend.objects.all().values()
    
    user = Edit.objects.last()
    response = {'profile_pict' : user.img, 'user_name' : user.name, "Add_Friend_Form" : Add_Friend_Form, "Friend" : all_friend}
    return render(request, 'friends-base.html', response)
	
def add_friend(request):
	form = Add_Friend_Form(request.POST or None)
	if(request.method == 'POST' and 'herokuapp.com' in request.POST['heroku'] and url_is_valid(request.POST['heroku'])):
		# response['name'] = request.POST['name']
		# response['heroku'] = request.POST['heroku']
		friend = Friend(name=request.POST['name'], heroku=request.POST['heroku'])
		friend.save()
		return redirect('/friends/', permanent = True)
	else:
		return redirect('/friends/')

def url_is_valid(url):
	request = urllib.request.Request(url)
	request.get_method = lambda: 'HEAD'

	try:
		urllib.request.urlopen(request)
		return True
	except urllib.request.HTTPError:
		return False
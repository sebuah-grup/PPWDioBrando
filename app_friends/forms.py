from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    heroku = forms.URLField(label ='Heroku', required=True, widget=forms.URLInput(attrs=attrs))
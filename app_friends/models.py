from django.db import models

class Friend(models.Model):
	name = models.CharField(max_length=50)
	heroku = models.URLField()
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name + " : " + self.heroku

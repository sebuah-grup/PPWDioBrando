from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend
from .models import Friend
from .forms import Add_Friend_Form

# Create your tests here.
class FriendsUnitTest(TestCase):
	def test_friends_page_is_exist(self):
		response = Client().get('/friends/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/friends/')
		self.assertEqual(found.func, index)
		
	def test_model_can_create_new_friend(self):
		#Creating a new friend
		new_friend = Friend.objects.create(name='Joseph Jotaro', heroku = 'http://test.herokuapp.com')
		#Retrieving all available friends
		counting_all_available_friends= Friend.objects.all().count()
		self.assertEqual(counting_all_available_friends,1)

	def test_model_can_be_printed(self):
		new_activity = Friend.objects.create(name='Joseph Jotaro', heroku ='http://test.herokuapp.com')
		self.assertEqual(str(new_activity), 'Joseph Jotaro : http://test.herokuapp.com')

	def test_form_validation_for_blank_items(self):
		form = Add_Friend_Form(data={'name': '', 'heroku': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			["This field is required."]
		)
		
	def test_add_friend_fail(self):
		response = Client().post('/friends/', {'name': 'Anonymous', 'heroku': 'A'})
		self.assertEqual(response.status_code, 200)

		
	def test_add_friend_success_and_render_the_result(self):
		namaTest = 'Aragaki Ayase'
		herokuTest = 'http://aulia175.herokuapp.com'
		response = Client().post('/friends/add_friend/', {'name': namaTest, 'heroku': herokuTest}, follow = True)
		self.assertEqual(response.status_code, 200)
		html_response = response.content.decode('utf8')
		self.assertIn(namaTest,html_response)
		self.assertIn(herokuTest,html_response)
  
	def test_add_friend_wrong_url(self):
		namaTest = 'Aragaki Ayase'
		herokuTest = 'http://mylovelyangelayase.herokuapp.com'
		response = Client().post('/friends/add_friend/', {'name': namaTest, 'heroku': herokuTest}, follow = True)
		self.assertEqual(response.status_code, 200)
		html_response = response.content.decode('utf8')
  

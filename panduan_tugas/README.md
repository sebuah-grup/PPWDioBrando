# Panduan Khusus Pengerjaan Tugas 1 PPW untuk Tim _project-diobrando_

* * *

## DISCLAIMER
README ini perlu dibaca untuk menjaga konsistensi pengerjaan tugas antar sesama anggota tim.
Gunakan template yang ada dengan baik :)

## Hal yang Perlu Diperhatikan
Lakukan pull terlebih dahulu sebelum mulai bekerja, untuk menghindari _conflict_.

## Struktur App pada Project
Semua app disini pada dasarnya memiliki nama yang dimulai dengan prefix `app_`
Untuk membuat app baru, gunakan script: `python manage.py startapp app_<masukkan nama app disini>`

## Testing Secara Lokal
Lakukan command berikut ini di CMD:
1. Untuk testing: `coverage run --include='app_*/*' manage.py test`
2. Untuk cek coverage: `coverage report -m`
3. Selalu lakukan `python manage.py collectstatic` ketika ada masalah pada html anda.
4. Bila mendapatkan **ValueError : Missing staticfiles...** gunakan: `python manage.py collectstatic`

## Struktur Template HTML
### Jenis-jenis Template dan Header HTML-nya
Secara umum, terdapat empat level template, yaitu:
1. `template\base.html` (template umum project),
2. `app_<nama app>\template\<nama app>-base.html` (template app),
3. `app_<nama app>\template\<nama app>-<nama page>.html` (template page), dan
4. `app_<nama app>\template\partials\<nama app>-<nama bagian>.html` (template partial, untuk bagian tertentu seperti header).

Berikut ini adalah format header untuk template umum project (level 1):
```
{% load staticfiles %}
```

Berikut ini adalah format header untuk HTML template app (level 2):
```
{% extends 'base.html' %}
{% load staticfiles %}
```

Dan ini adalah format header untuk HTML template page (level 3):
```
{% extends '<nama app>-base.html' %}
{% load staticfiles %}
```

**WARNING: Pastikan {% load staticfiles %} ada di header setiap HTML template jenis level 1 s/d 3**

### Struktur Isi Template HTML
Format standar suatu __template block__:
```
{% block nama_block %}
	<!–– Masukkan konten disini ––>\
{% endblock %}
```

Berikut ini adalah nama block yang tersedia untuk digunakan pada jenis template app dan page:
1.  `meta` digunakan untuk menyisipkan meta (contoh: description, viewport, dsb.) ke dalam template, selain meta yang ada di template umum.
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app (level 2).
2.  `stylescript` digunakan untuk mengganti versi bootstrap css dari versi default (4.0.0-beta1).
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app (level 2).
3.  `stylescript_app` digunakan untuk menambah CSS yang dikhususkan pada suatu app.
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app (level 2).
4.  `stylescript_page` digunakan untuk menambah CSS yang dikhususkan pada suatu page.
    Block ini dideklarasikan di template app (level 2), dapat diimplementasikan di template page (level 3).
5.  `title` digunakan untuk memasukkan judul pada suatu app atau page.
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app/page (level 2 atau 3).
6.  `header` digunakan untuk memasukkan header (biasanya navbar).
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan dari template umum sampai template page (level 1 s/d 3).
7.  `content` digunakan untuk memasukkan konten utama.
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app/page (level 2 atau 3).
8.  `content_page` digunakan untuk memasukkan konten utama pada page, namun tidak mengganti keseluruhan konten default app.
    Block ini dideklarasikan di template page (level 2, dalam block `content`), dapat diimplementasikan di template app/page (level 2 atau 3).
9.  `footer` digunakan untuk memasukkan footer (biasanya tanda copyright).
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan dari template umum sampai template page (level 1 s/d 3).
10. `scripts` digunakan untuk memasukkan file Javascript yang versinya berbeda dari template umum (contoh: Bootstrap js, JQuery.min).
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app/page (level 2 atau 3).
11. `scripts_app` digunakan untuk memasukkan Javascript yang khusus digunakan untuk app.
    Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app (level 2).
12. `scripts_page` digunakan untuk memasukkan Javascript yang khusus digunakan untuk page.
    Block ini dideklarasikan di template app (level 2), dapat diimplementasikan di template page (level 3).
13. ~~`jquery` digunakan untuk memasukkan script JQuery pada app/page.~~
    ~~Block ini dideklarasikan di template umum (level 1), dapat diimplementasikan di template app/page (level 2 atau 3).~~
    **Note: block jquery belum digunakan untuk saat ini**
14. Apabila ada block lain yang ingin digunakan, silahkan definisikan sendiri :)
    Contoh: `content_sesuatu`, `isi_apa_ya`

Untuk lebih jelasnya, silahkan buka `templates/base.html`

Berikut ini adalah contoh template app (level 2):
```
{% extends "base.html" %}
{% load staticfiles %}
{% block meta %} <!–– Masukkan tag meta disini ––> {% endblock %}
{% block title %} <!–– Masukkan judul page disini ––> {% endblock %}

<!–– Note: stylescript hanya digunakan apabila versi bootstrap css yang ingin digunakan berbeda versi, otherwise JANGAN DIGUNAKAN! ––>
{% block stylescript %} <!–– Masukkan tag link ke CSS bootstrap disini ––> {% endblock %}

{% block stylescript_app %} <!–– Masukkan tag link untuk CSS khusus app disini ––> {% endblock %}
{% block stylescript_page %}{% endblock %}

{% block header %} {% include "partials/header.html" %} {% endblock %}
{% block content %}
	<!–– Masukkan konten default app disini (dalam block content) ––>
	<!–– Apabila dibutuhkan, gunakan content_page untuk menampung konten khusus page ––>
	{% block content_page %}{% endblock %}
{% endblock %}
{% block footer %} {% include "partials/footer.html" %} {% endblock %}

<!–– Note: scripts digunakan apabila ada JS yang berbeda versi, otherwise JANGAN DIGUNAKAN! ––>
{% block scripts %} <!–– Masukkan tag JS yang ingin digunakan namun berbeda versi ––> {% endblock %}

{% block scripts_app %} <!–– Masukkan script JS yang ingin digunakan app disini ––> {% endblock %}
{% block scripts_page %}{% endblock %}
{% block jquery %} <!–– Apabila ada script jquery khusus, masukkan disini ––> {% endblock %}
```

Berikut ini adalah contoh template page (level 3):
```
{% extends "<nama app>-base.html" %}
{% load staticfiles %}
<!–– Apabila ingin menggunakan meta tambahan, COPY isi block meta yang dibutuhkan di template app, lalu tambahkan sesuai keinginan ––>
{% block meta %} <!–– Masukkan tag meta disini ––> {% endblock %}
{% block title %} <!–– Masukkan judul page disini ––> {% endblock %}

{% block stylescript_page %} <!–– Masukkan tag link untuk CSS khusus page disini ––> {% endblock %}

<!–– APABILA INGIN MENIMPA CONTENT DEFAULT PADA APP, gunakan ini: ––>
{% block content %} <!–– Masukkan konten page disini ––> {% endblock %}
<!–– OTHERWISE, gunakan ini: ––>
{% block content_page %} <!–– Masukkan konten page disini ––> {% endblock %}

{% block scripts_page %} <!–– Masukkan script JS yang ingin digunakan page disini ––> {% endblock %}
{% block jquery %} <!–– Apabila ada script jquery khusus, masukkan disini ––> {% endblock %}
```

### Struktur Isi Template CSS
Secara umum, terdapat tiga level CSS, yaitu:
1. `static\css\base.css` (CSS umum),
2. `app_<nama app>\static\css\<nama app>.css` (CSS app), dan
3. `app_<nama app>\static\css\<nama app>-<nama page>.css` (CSS page).

Berikut ini adalah contoh template CSS yang dapat digunakan:
```
/* Font family definition: you can add custom fonts here */


/* Animation definitions */


/* CSS definition for regular screen size */
/* Main body */

/* Navbar */

/* Content */


/* Special rules for certain screen sizes, size-min: ..., size-max: ... */
/* Main body */

/* Navbar */

/* Content */


/* And so on........ */
```

* * *

Created on 5 Oct 2017, by Ichlasul Affan.